 # merge sort
 # reference : http://qiita.com/woolon/items/4cff50de7e697cb2664f
import random

list = []
n = 10
x = 1
y = 100

for i in range(n):
    list.append(random.randint(x, y))
print(list)


def merge_sort(lst):
    length = len(lst)
    if length > 1:
        left = merge_sort(lst[:int(length/2)])
        right = merge_sort(lst[int(length/2):])
        lst = []
        while len(left) != 0 and len(right) != 0:   
            if left[0] < right[0]:                  
                lst.append(left.pop(0))           
            else:                                   
                lst.append(right.pop(0))          
        if len(left) != 0:                          
            lst.extend(left)                      
        elif len(right) != 0:                      
            lst.extend(right)                     
    return lst                                    

print(merge_sort(list))
