
import sys


for arg in sys.argv:
    print(arg)
print("")

argv = sys.argv
# print(argv[1])

argc = len(argv)
if (argc != 2):
    print('put one argument from natural number : ')
    quit()


n = int(argv[1])
# n = 10
output_list = []
tmp_list = []

def fib_2(n):
    if n == 1:        
        tmp_list.append(1)
        return 1
    elif n == 2:
        tmp_list.append(1)
        tmp_list.append(1)
        return 1
    else:
        tmp_list.append(1)
        tmp_list.append(1)       
        for i in range(2, n):
            tmp_list[n] = tmp_list[n-1] + tmp_list[n-2]
        return tmp_list[n]
    
for i in range(1, n+1):
    output_list.append(fib_2(i))

print(output_list)
