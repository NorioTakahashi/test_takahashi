 # 170424_ex67.py
 # execute "python 170424_ex67.py < 200501a.jmn"
 # coding: utf-8
from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

data = ""
list_midasi = []
list_hinsi = []

for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            list_midasi.append(mrph.midasi)
            list_hinsi.append(mrph.hinsi)
        data = ""
# print(list_midasi)
# print(list_hinsi)

for i in range(len(list_midasi)):
    if list_hinsi[i] == "名詞":
        if list_midasi[i+1] == "の":
            if list_hinsi[i+2] == "名詞":
                print("".join(list_midasi[i:i+3]))
