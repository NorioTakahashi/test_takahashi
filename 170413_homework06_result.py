"Eratosthenes' sieve"の方が、"Brute Force"より計算時間が長い。
何かが間違っているはず。

	before		after
	Eratos	Brute	Eratos(after improvement)
1	0.4485	0.0215	0.0306219
2	0.4841	0.0217	0.031742
3	0.4549	0.0230	0.030583
4	0.4903	0.0210	0.028039
5	0.4788	0.0220	0.030483
ave.[s]	0.4713	0.0219	0.0303
