for i in range(0, 10):
    if i == 0:
        print("", end="   ")
        for j in range(1, 10):
            print("%3d" % j, end="")
        print("")
    else:
        print("%3d" % i, end="")
        for j in range(1, 10):
            k = i * j
            print("%3d" % k, end="")
        print("")
