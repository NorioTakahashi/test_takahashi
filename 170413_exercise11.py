# make an alphabet_list
alphabet = "abcdefghijklmnopqrstuvwxyz"
alphabet_list = []

for i in range(len(alphabet)):
    alphabet_list.append(alphabet[i])
 # print(alphabet_list)

# decide how to encrypt input_string : change alphabet number
n = 5
encrypt_list = []
for i in range(len(alphabet)):
    m = i + n
    if m < len(alphabet):
        encrypt_list.append(alphabet[m])
    else:
        encrypt_list.append(alphabet[m - len(alphabet)])
 # print(encrypt_list)

# input_string
string = "hypertext"    

# make an input_list
input_list = []
for i in range(len(string)):
    input_list.append(string[i])
print(input_list)
 
# encrypt input_string : change alphabet number
output_list = []
for alp in input_list:
    for j in range(len(alphabet_list)):
        if alp == alphabet_list[j]:
            output_list.append(encrypt_list[j])
print(output_list)
            
# decrypt output_string
decrypt_list = []
for i in range(len(alphabet)):
    m = i - n
    if m > -1:
        decrypt_list.append(alphabet[m])
    else:
        decrypt_list.append(alphabet[m + len(alphabet)])
 # print(decrypt_list)

original_list = []
for alp in output_list:
    for j in range(len(alphabet_list)):
        if alp == alphabet_list[j]:
            original_list.append(decrypt_list[j])
print(original_list)
