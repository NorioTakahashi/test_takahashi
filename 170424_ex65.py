 # 170424_ex65.py
 # execute "python 170424_ex65.py < 200501a.jmn"
 # coding: utf-8
from pyknp import Jumanpp
import sys
import re

jumanpp = Jumanpp()

data = ""
n_total = n_verb = n_adj = n_adj_verb = 0

for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            n_total += 1
            if mrph.hinsi == "動詞":
                n_verb += 1
            elif mrph.hinsi == "形容詞":
                if re.search("イ形容詞", mrph.katuyou1):
                    n_adj += 1
                elif re.search("ナ形容詞", mrph.katuyou1):
                    n_adj_verb += 1
                else:
                    pass
            else:
                pass
        data = ""
# print(list)
prob = 0
prob = (n_verb + n_adj + n_adj_verb)/(n_total)
print("{0:.4f}".format(prob)) 
