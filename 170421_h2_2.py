# execute the following command on linux
# -> "python3 170421_h2_2.py doc0000000000.txt"

import sys

filename = sys.argv[1]

# unigram
unigram = []

with open(filename) as my_file:
    for line in my_file:
        unigram.extend(line.strip().split())        
        # print((line.strip().split()))
# print(unigram)
        
# bigram
bigram = []
for i in range(1, len(unigram)):
    cw = []
    cw = unigram[i-1] + ", " + unigram[i]
    bigram.append(cw)
# print(bigram)


# count unigrams and bigrams

f_unigram = f_The = f_man = f_is = f_in = f_the = f_house = 0
f_bigram = f_The_man = f_man_is = f_is_in = f_in_the = f_the_house = 0

f_unigram = len(unigram)
f_The = unigram.count("The")
f_man = unigram.count("man")
f_is = unigram.count("is")
f_in = unigram.count("in")
f_the = unigram.count("the")
f_house = unigram.count("house")
print(f_unigram)
print(f_The)

f_bigram = len(bigram)
f_The_man = bigram.count("The, man")
f_man_is = bigram.count("man, is")
f_is_in = bigram.count("is, in")
f_in_the = bigram.count("in, the")
f_the_house = bigram.count("the, house")
print(f_bigram)
print(f_The_man)

prob = 0
prob = (f_The/f_unigram)*(f_The_man/f_The)*(f_man_is/f_man)*(f_is_in/f_is)\
*(f_in_the/f_in)*(f_the_house/f_the)

print("Probability = ", prob)
