 # quick sort
 # reference : http://www.lifewithpython.com/2015/01/python-sample-quick-sort.html

import random

list = []
n = 10
x = 1
y = 100

for i in range(n):
    list.append(random.randint(x, y))
print(list)


def quick_sort(lst):
    if len(lst) < 2:
        return lst
    else:
        pivot = lst[0]
        lst_rest = lst[1:]
        smaller = []
        larger = []
        for i in range(len(lst_rest)):
            if lst_rest[i] < pivot:
                smaller.append(lst_rest[i])
            else:
                larger.append(lst_rest[i])
        return quick_sort(smaller) + [pivot] + quick_sort(larger)
print(quick_sort(list))
