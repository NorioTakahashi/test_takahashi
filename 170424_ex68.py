 # 170424_ex68.py
 # execute "echo "今日のお昼はお弁当だった." | python 170424_ex68.py"
 # coding: utf-8
import sys
from pyknp import KNP 

input_sentence = sys.stdin.readline() # 改行を含む, string型

# インスタンスを作成
knp = KNP(jumanpp=True)

# 文を解析し、解析結果を Python の内部構造に変換して result に格納
result = knp.parse(input_sentence)

# 解析結果へのアクセス
for bnst in result.bnst_list():
    print("".join(mrph.midasi for mrph in bnst.mrph_list()), end=" ")
print("")
