import time

t0 = time.clock()   # time(t0)

prime_num = []
max = 1001

for i in range(2, max):
    for j in range(2, i):
        if i % j == 0:
            break
        else:
            continue
    else:
        prime_num.append(i)
print(prime_num)           
               
t1 = time.clock()   # time(t1)
print("dt="+str(t1-t0)+"[s]") 
