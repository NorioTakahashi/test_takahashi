 # 170428_ex77.py
 # execute "python 170428_ex77.py < 200501a.knp"
 # 一部の解析結果に間違いあり。
 # ”避難所の日々の暮らし方”と、”予知なしの冬の午後５時”はOK。
 #coding: utf-8
from pyknp import KNP
import sys

knp = KNP()

data = ""
sentence = []
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
#        for bnst in result.bnst_list():
#            print(bnst.repname)
        for bnst_1, bnst_2 in zip(result.bnst_list(), result.bnst_list()[1:]):
            parent = bnst_1.parent
            if bnst_1.mrph_list()[-1].midasi == "の" and bnst_1.mrph_list()[-2].hinsi == "名詞" \
and bnst_2.mrph_list()[0].hinsi == "名詞":
#                print(bnst_2.mrph_list()[0].midasi)
                if parent.bnst_id - bnst_1.bnst_id > 1:
                    print("".join(mrph.midasi for mrph in bnst_1.mrph_list()))
                    print(bnst_1.parent_id)
                    print(bnst_1.fstring)
                    print("".join(mrph.midasi for mrph in bnst_2.mrph_list()))
                    print(bnst_2.parent_id)
                    print(bnst_2.fstring)
                    for bnst in result.bnst_list():
                        sentence.append("".join(mrph.midasi for mrph in bnst.mrph_list()))
                    print(" ".join(sentence))
                    print("")
                    data = ""
                    sentence = []
        data = ""
