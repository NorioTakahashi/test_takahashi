for n in range(1, 31):
    if n % 3 == 0 and n % 5 == 0 :
        x = "FizzBuzz"
        print(x, end=", ")
    elif n % 3 == 0 and n % 5 != 0 :
        x = "Fizz"
        print(x, end=", ")
    elif n % 3 != 0 and n % 5 == 0 :
        x = "Buzz"
        print(x, end=", ")
    else:
        print(n, end=", ")
print("")
