import sys


for arg in sys.argv:
    print(arg)
print("")

a = [1, 2, 3, 4, 5, 6, 7]
b = ["Monday", "Tuesday", "Wednesday",
    "Thursday", "Friday", "Saturday", "Sunday"]

argv = sys.argv
# print(argv[1])
# print(type(argv[1]))

argc = len(argv)
if (argc != 2):
    print('put one argument from ' +
          'the following list : ', a)
    quit()

n = int(argv[1])
i = 0
judge = False

while i < 7:
    # print(i)
    # print(a[i])
    # print(a[i] == n)
    if a[i] == n:
        print(b[i])
        judge = True
        break
    i += 1

if judge == False: 
    print('put one argument from ',
         'the following list : ', a)
