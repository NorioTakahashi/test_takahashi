import time

t0 = time.clock()   # time(t0)

prime_num = []
max = 1001
upper_limit = 0

for i in range(2, max):
    prime_num.append(i)

for i in range(2, max):
    if i not in prime_num:
        continue
    else:
        upper_limit = max // i
        # print(upper_limit)
        for j in range(2, upper_limit + 1):
            if j * i not in prime_num:
                pass
            else:
                prime_num.remove(j* i)
print(prime_num)

t1 = time.clock()   # time(t1)
print("dt="+str(t1-t0)+"[s]") 
