 # 170424_ex70.py
 # execute "python 170424_ex70.py < 200501a.knp"
 # coding: utf-8
from pyknp import KNP
import sys

knp = KNP(jumanpp=True)
data = ""
n = 0

for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        for bnst in result.bnst_list():
            n = 0
            for mrph in bnst.mrph_list():
                if mrph.hinsi == "名詞":
                    n += 1
                    if n == 2:
                        print("".join(mrph.midasi for mrph in bnst.mrph_list()))
                        break
                    else:
                        pass
        data = ""
