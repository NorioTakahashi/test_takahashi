 # 170428_ex73.py
 #coding: utf-8
from pyknp import KNP

# インスタンスを作成
knp = KNP()

# 文を解析し、解析結果を Python の内部構造に変換して result に格納
result = knp.parse("石油は揮発性液体であるので、注意する必要がある。")

judge_1 = False
judge_2 = False

for bnst in result.bnst_list():
    for mrph in bnst.mrph_list():
        if "名詞" == mrph.hinsi:
            judge_1 = True
        elif "接尾辞" == mrph.hinsi:
            judge_2 = True
        else:
            continue
        if judge_1 == True and judge_2 == True:
            print("正規化代表表記:", bnst.repname)
            judge_1 = False
            judge_2 = False
            break
        else:
            pass
    judge_1 = False
    judge_2 = False
