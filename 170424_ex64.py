 # 170424_ex64.py
 # execute "python 170424_ex64.py < 200501a.jmn"
 # coding: utf-8
from pyknp import Jumanpp
import sys
import collections

jumanpp = Jumanpp()

data = ""
list = []

for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            list.append(mrph.genkei)
        data = ""
# print(list)
count_dict = collections.Counter(list)
print(count_dict)
