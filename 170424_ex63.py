 # 170424_ex63.py
 # execute "python 170424_ex63.py < 200501a.jmn"
 # coding: utf-8
from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

data = ""

for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            if mrph.hinsi == "動詞":
                print(mrph.genkei)
        data = ""
