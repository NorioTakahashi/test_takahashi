import sys


for arg in sys.argv:
    print(arg)
print("")

argv = sys.argv
# print(argv[1])

argc = len(argv)
if (argc != 2):
    print('put one argument from natural number : ')
    quit()

n = int(argv[1])
output_list = []

def fib(n):
    if n > 1:
        return fib(n-1) + fib(n-2)
    else:
        return 1
    
for i in range(0, n):
    output_list.append(fib(i))

print(output_list)
