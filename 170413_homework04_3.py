4-3

再帰呼出しを用いた場合の方が、再帰呼出しを用いない場合より優れている。
メリットは、以下のとおり。
１：計算用メモリが少なくて済む。
　→直前の二つの値、"fib(n-1)"および"fib(n-2)"をメモリに保存しておけば良い。
　→再帰呼出しを用いない場合、一時保存用リストが必要
２：計算用プログラムの行数が短い。
