 # eight queen
 # reference : http://www.geocities.jp/m_hiroi/light/python03.html
board = []
count = 0


def check(n):
    for y in range(1, n):
        if conflict(board[y], y):
            return False
    return True


def conflict(x, y):
    for y1 in range(0, y):
        x1 = board[y1]
        if x1 - y1 == x - y or x1 + y1 == x + y:
            return True
    return False


def queen(n, y=0):
    global count
    if n == y:
        if check(n):
            print(board)
            count += 1
            print(count)
    else:
        for x in range(0, n):
            if x in board:
                continue
            board.append(x)
            # print(board)
            queen(n, y + 1)
            # print(board)
            board.pop()
queen(8, y=0)
