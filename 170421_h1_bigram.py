# execute the following command on linux
# -> "python3 170421_h1_bigram.py doc0000000000.txt | more""

import sys

filename = sys.argv[1]

# unigram
unigram = []

with open(filename) as my_file:
    for line in my_file:
        unigram.extend(line.strip().split())        
        # print((line.strip().split()))
# print(unigram)
        
# bigram
bigram = []
for i in range(1, len(unigram)):
    cw = []
    cw = unigram[i-1] + ", " + unigram[i]
    bigram.append(cw)
print(bigram)
