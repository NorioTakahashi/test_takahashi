 # 170424_ex66.py
 # execute "python 170424_ex66.py < 200501a.jmn"
 # coding: utf-8
from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

data = ""
list_midasi = []
list_bunrui = []

for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            list_midasi.append(mrph.midasi)
            list_bunrui.append(mrph.bunrui)
        data = ""
# print(list_midasi)
# print(list_bunrui)

for i in range(len(list_midasi)):
    if list_bunrui[i] == "サ変名詞":
        if list_midasi[i+1] == "する" or list_midasi[i+1] == "できる":
            print(list_midasi[i], list_midasi[i+1])
